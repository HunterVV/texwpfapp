﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TexWpfApp
{
    class MainViewModel : INotifyPropertyChanged
    {
        public List<Item> ItemsCollection { get; set; }

        private Item selected_item;
        public Item SelectedItem
        {
            get { return selected_item; }
            set
            {
                selected_item = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public MainViewModel()
        {
            ItemsCollection = ItemsWorker.GetItems(); 
        }

        private TemplateCommand update_command;
        public TemplateCommand UpdateCommand
        {
            get
            {
                return update_command ?? (update_command = new TemplateCommand(obj =>
                {
                    ItemsCollection = ItemsWorker.GetItems();
                    OnPropertyChanged("ItemsCollection");
                    SelectedItem = null;
                }));
            }
        }

        private TemplateCommand add_command;
        public TemplateCommand AddCommand
        {
            get
            {
                return add_command ?? (add_command = new TemplateCommand(obj =>
                  {
                      ItemsWorker.PostItem(new Item() { Name = "New name" });
                      ItemsCollection = ItemsWorker.GetItems();
                      OnPropertyChanged("ItemsCollection");
                      if (ItemsCollection.Count != 0)
                      {
                          SelectedItem = ItemsCollection.Last();
                      }
                  }));
            }
        }

        private TemplateCommand remove_command;
        public TemplateCommand RemoveCommand
        {
            get
            {
                return remove_command ?? (remove_command = new TemplateCommand(obj =>
                {
                    ItemsWorker.DeleteItem(SelectedItem.Id);
                    ItemsCollection = ItemsWorker.GetItems();
                    OnPropertyChanged("ItemsCollection");
                    SelectedItem = null;
                }, obj => SelectedItem != null));
            }
        }

        private TemplateCommand setimage_Command;
        public TemplateCommand SetImageCommand
        {
            get
            {
                return setimage_Command ?? (setimage_Command = new TemplateCommand(obj =>
                  {
                      try
                      {
                          OpenFileDialog dialog = new OpenFileDialog();
                          dialog.Filter = "Png images(*.jpg)|*.jpg";
                          if (dialog.ShowDialog() ?? false)
                          {
                              string image_path = dialog.FileName;
                              SelectedItem.Image = new Bitmap(image_path);
                          }
                      }
                      catch (Exception e)
                      {
                          Console.WriteLine(e.Message);
                      }
                  }, obj => SelectedItem != null));
            }
        }

        private TemplateCommand sort_Command;
        public TemplateCommand SortCommand
        {
            get
            {
                return sort_Command ?? (sort_Command = new TemplateCommand(par =>
                {
                    string sort_type = (string)par;
                    switch (sort_type)
                    {
                        case "Id":
                            ItemsCollection = ItemsCollection.OrderBy(x => x.Id).ToList();
                            break;
                        case "Name":
                            ItemsCollection = ItemsCollection.OrderBy(x => x.Name).ToList();
                            break;
                    }
                    OnPropertyChanged("ItemsCollection");
                }, obj => ItemsCollection.Count != 0));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
