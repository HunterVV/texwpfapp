﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace TexWpfApp
{
    [DataContract]
    class Item : INotifyPropertyChanged
    {
        [IgnoreDataMember]
        public bool IsNew = true;

        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "Name")]
        private string name;
        [IgnoreDataMember]
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        [IgnoreDataMember]
        public BitmapImage image_source { get; set; }
        [IgnoreDataMember]
        public BitmapImage ImageSource
        {
            get
            {
                return image_source;
            }
            set
            {
                image_source = value;
                OnPropertyChanged("ImageSource");
            }
        }

        [IgnoreDataMember]
        private Bitmap image;
        [IgnoreDataMember]
        public Bitmap Image
        {
            get { return image; }
            set
            {
                image = value;

                if (image != null)
                {
                    using (MemoryStream memory = new MemoryStream())
                    {
                        image.Save(memory, ImageFormat.Png);
                        memory.Position = 0;
                        BitmapImage bitmap_image = new BitmapImage();
                        bitmap_image.BeginInit();
                        bitmap_image.StreamSource = memory;
                        bitmap_image.CacheOption = BitmapCacheOption.OnLoad;
                        bitmap_image.EndInit();
                        ImageSource = bitmap_image;
                    }
                }
            }
        }

        [DataMember(Name = "ImageByteArray")]
        public byte[] ImageByteArray
        {
            get
            {
                if (Image != null)
                {

                    using (var stream = new MemoryStream())
                    {
                        Image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        stream.Close();
                        return stream.ToArray();
                    }
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                {
                    using (MemoryStream stream = new MemoryStream(value))
                    {
                        Image = new Bitmap(stream);
                    }
                }
                else
                {
                    Image = null;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (!IsNew)
            {
                ItemsWorker.PutItem(this);
            }
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
