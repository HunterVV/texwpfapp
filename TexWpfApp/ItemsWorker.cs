﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Newtonsoft.Json;
using System.Windows;

namespace TexWpfApp
{
    static class ItemsWorker
    {
        public static string Address = ConfigurationManager.AppSettings["AddressApiItems"] ?? "";

        public static List<Item> GetItems()
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Address);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                List<Item> list = new List<Item>();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        byte[] bytes;
                        byte[] buffer = new byte[16 * 1024];
                        using (MemoryStream ms = new MemoryStream())
                        {
                            int read;
                            while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                ms.Write(buffer, 0, read);
                            }
                            bytes = ms.ToArray();
                        }
                        string json_response = Encoding.UTF8.GetString(bytes);
                        list = JsonConvert.DeserializeObject<List<Item>>(json_response);
                    }
                }

                for (int i = 0; i < list.Count; i++)
                {
                    list[i].IsNew = false;
                }
                return list;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show(e.Message);
                return new List<Item>();
            }
        }

        public static Item GetItem(int id)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Concat(Address, id));
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Item item = new Item();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        byte[] bytes;
                        byte[] buffer = new byte[16 * 1024];
                        using (MemoryStream ms = new MemoryStream())
                        {
                            int read;
                            while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                ms.Write(buffer, 0, read);
                            }
                            bytes = ms.ToArray();
                        }
                        string json_response = Encoding.UTF8.GetString(bytes);
                        item = JsonConvert.DeserializeObject<Item>(json_response);
                    }
                }

                return item;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show(e.Message);
                return null;
            }
        }

        public static void PostItem(Item item)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Address);
                request.Method = "POST";
                using (Stream stream = request.GetRequestStream())
                {
                    string json_request = JsonConvert.SerializeObject(item);
                    byte[] bytes = Encoding.UTF8.GetBytes(json_request);
                    stream.Write(bytes, 0, bytes.Length);
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show(e.Message);
            }
        }

        public static void PutItem(Item item)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Concat(Address, item.Id));
                request.Method = "PUT";
                using (Stream stream = request.GetRequestStream())
                {
                    string json_request = JsonConvert.SerializeObject(item);
                    byte[] bytes = Encoding.UTF8.GetBytes(json_request);
                    stream.Write(bytes, 0, bytes.Length);
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show(e.Message);
            }
        }

        public static void DeleteItem(int id)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Concat(Address, id));
                request.Method = "DELETE";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show(e.Message);
            }
        }
    }
}
